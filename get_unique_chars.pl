#!/usr/bin/perl
use 5.014;
use strict;
use warnings;

use Data::Dumper; 
use utf8;
use Text::Unidecode;
use Encode; 

###########################################################################################################################
###########################################################################################################################
BEGIN 
{
    binmode STDOUT, ":utf8"; # set STDOUT to utf8
    select((select(STDOUT), $|=1)[0]); # Disable STDOUT buffering
}

# Folders containing Input/Output files
my $input_folder = "INPUTS/"; 
my $output_folder = "OUTPUTS/"; 

#  FILE DESCRIPTIONS 
#  =========================================================================================================================
#           FILE             | I/O |                        DESCRIPTION                                                
#  =========================================================================================================================
#  jp_strings.txt            |  I  | japanese translations to be encoded, pulled from Excel spreadsheet
#  jp_ascii_keys.txt         |  I  | ordered list of pairs of ascii keys used to represent (encode) a japanese character 
#  20x20bitmaps.bdf          |  I  | kappa20 japanese font file 
#  JIS-0208.txt              |  I  | Conversion chart between JIS and Unicode 
#  jp_unique_chars.txt       |  O  | list of unique japanese characters present in input file
#  jp_str_lens_COPYPASTE.txt |  O  | lengths (number of ascii characters) of each ENCODED japanese string 
#  jp_bitmap_COPYPASTE.txt   |  O  | ordered list of hex values to populate japanes_fonts[] array in font.c
#  jp_character_map.txt      |  O  | human-readable mapping that details the encoding for each japanese character 
#  japanese.txt              |  O  | output file containing japanese encoded strings, to be stored on SD card
#  =========================================================================================================================
#
my $f_asciicodes = $input_folder."jp_ascii_keys.txt";
my $f_jptranslations = $input_folder."jp_strings.txt"; 
my $f_bitmap = $input_folder."20x20bitmaps.bdf";
my $f_JIS = $input_folder."JIS-0208.txt";
my $f_jpstringlengths = $output_folder."jp_str_lens_COPYPASTE.txt"; 
my $f_jpuniquechars = $output_folder."jp_unique_chars.txt";
my $f_bitmap_copypaste = $output_folder."jp_bitmap_COPYPASTE.txt";
my $f_jpcharmap = $output_folder."jp_character_map.txt";
my $f_output = $output_folder."japanese.txt"; 

# global variables
my @strs; # Holds each japanese string
my @strlens; # Holds lengths (in unichode chars) of each japanese string
my @unique_chars; # sorted list of each unique character 
my @unicodes_d; # array of decimal unicode values corresponding to @unique_chars 
my @unicodes_x; # array of hex unicode values corresponding to @unique_chars 
my %JIS_lookup; # hash map for unicode-->JIS
my %charMap; # FINAL hash map holding all data
my %asciiMap; # map containing asci keys --> jp character

# Constant declarations for indexing into %charMap hash
use constant CHARCODE => 0;
use constant UNICODE_D => 1;
use constant UNICODE_X => 2;
use constant JIS_D => 3;
use constant JIS_X => 4;
use constant BITMAP => 5;

# Call main program driver function 
&createJPCharMap;

END 
{
    print "\n";
};
###########################################################################################################################
###########################################################################################################################
#                                               BEGIN SUBROUTINES
###########################################################################################################################
###########################################################################################################################


#
# Converts decimal to hex
#
sub d2x
{
    my @retvals;
    my @args = @_;
    foreach my $arg (@args)
    {
        push(@retvals,  sprintf("0x%X", $arg));
    }
    return @retvals; 
}


#
# Converts hex to decimal
#
sub x2d
{
    my @retvals;
    my @args = @_;
    foreach my $arg (@args)
    {
        push(@retvals, $arg);
    }
    return \@retvals; 
}


#
# Prints length of 1-per-line translation file into lengths text file
#
sub getStringLengths
{
    no warnings; 
    my @strlens;
    my ($num_reg_chars, $num_jp_chars);
    my $reg_char_multiplier=2; 
    my $jp_char_multiplier=2; 
    my ($templen,$len); 

    # Store string lengths in array @strlens
    open my $fh, "<:utf8", $f_jptranslations or die "error opening $f_jptranslations";
    while (<$fh>) 
    {
        # Strip newline and push onto @strs array 
        chomp $_ and push(@strs, $_);
        $num_reg_chars = $_ =~ tr/[^!-~\s]/[^!-~\s]/; # number of non-japanese characters 
        $num_jp_chars = length($_) - $num_reg_chars; # number of japanese characters 
        my $templen = length($_);
        
        # Make sure string length = sum of jp chars + sum of regular chars
        ($templen != $num_reg_chars + $num_jp_chars) and die "LENGTH IS FUCKED"; 

        # Calculate new string length for ascii representation
        $len = $num_reg_chars*$reg_char_multiplier + $num_jp_chars*$jp_char_multiplier and push(@strlens, $len);
    };
    close $fh or die "error closing $f_jptranslations"; 
    return @strlens; 
}


#  getuniquecharsunicode()
#  
#  Description: subroutine that gets each unique character from japanese translations file and assigns it a unique 
#               two-character ascii code representation. this 2-byte code will correspond to a line number that 
#               contains the 20x20 pixel hex image of that character
#
#  params: none
#  return: (\@unique_chars, \@unicodes_d, \@unicodes_x); 
#          @unique_chars: array of unique japanese characters
#          @unicodes_d: array of decimal unicode values for unique chars
#          @unicodes_: array of hex unicode values for unique chars
#
sub getUniqueCharsUnicode
{
    # store each character in every japanese word into array @chars
    open my $fh_jptranslations, "<:utf8", $f_jptranslations or die "error reading file: <$f_jptranslations>"; 
    my (@chars, @tempchars);
    while (<$fh_jptranslations>) 
    {
        $_ =~ s/^\s+|\s+$// and @tempchars=split('',$_) and push(@chars,@tempchars); 
    }
    close $fh_jptranslations or die "error closing file <$f_jptranslations>";

    # store unique characters as hash keys
    my %unique_jpchars = map { $_ => ord $_ } @chars;
    @unique_chars = sort keys %unique_jpchars;

    # Push unicode values onto arrays 
    foreach my $char (@unique_chars)
    {
        my $newchar;
        my $ordchar = ord($char);
        if ( (ord($char) < 200) and (ord($char) != 32) and (ord($char) != 45))
        {
            $newchar = 65313 + (ord($char)-ord("A")); 
        }
        elsif( ord($char) == 32 ) 
        {
            $newchar = 12288; # unicode ideographic space
        }
        elsif( ord($char) == 45 )  
        {
            $newchar = 8208;
        }
        else
        {
            $newchar=ord($char); 
        }
        push( @unicodes_d, $newchar) and push(@unicodes_x, d2x($newchar));  
    }
    return (\@unique_chars, \@unicodes_d, \@unicodes_x); #return array of hex unicode values
}


#   getJISfromUnicode
#   
#   Description: subroutine to populate the global JIS_lookup hash, that enables conversion between unicode and JIS.
#                Hash keys are unicode hex numbers, and hash values are hex JIS numbers. This routine
#                stores codes for ALL possible characters, not just those used in the SP2
#
#   Params: None
#   Return: None
#
sub getJISfromUnicode
{
    #open file 
    open my $fh, "<", $f_JIS or die "Error opening file <$f_JIS>\n";
    my @fields;
    while (<$fh>)
    {
        /^#/ or @fields = split(" ", $_) and $JIS_lookup{$fields[2]} = $fields[1];
    }
    close $fh or die "Error closing file <$f_JIS>\n";
}


#
#   getASCIIkeys
#   
#   Description: subroutine to assign a two-character ascii encoding to each unique japanese character
#
#   Params: None
#   Return: None
#
sub getASCIIkeys
{
    # store ascii representations to array @ascii_codes
    open my $fh_ascii, "<:utf8", $f_asciicodes or die "error reading file: <$f_asciicodes>"; 
    my @codes;
    while (<$fh_ascii>) 
    {
        my @line = split(' ', $_);
        push(@codes, @line);
    }
    close $fh_ascii or die "error closing file <$f_jptranslations>";

   # store ascii codes as values in hash table and write to file
   my $i = 0; 
   foreach my $key (@unique_chars)
   {
       $asciiMap{$key} = $codes[$i];
       $i++; 
   }
}


#   getBitmap 
#
#   Description: retrieves the hex codes for a given character from the bitmap file
#
#   Params: $_[0] = JIS value for target character
#   Returns: \@pixels
#            reference to an array of bitmapped pixels for the character 
#
sub getBitmap
{
    my $target= $_[0]; # JIS value for target character 
    my @pixels;
    my @fields; # fields for each line
    my $flag = 0; # flag that implies character range has been found

    open my $fh, $f_bitmap or die "Error opening file <$f_bitmap>";
    while (<$fh>)
    {
        # If character has been found, set flag to 1
        /^ENCODING\b/ and @fields = split(" ", $_) and ($fields[1] == $target) and $flag=1;   

        # if flag is 1, grab corresponding bitmap pixels and reset flag to zero;
        if (($flag==1) and /^BITMAP\b/../^ENDCHAR\b/ ) 
        {
            m/^[\da-fA-F]+$/ and chomp $_ and push(@pixels,$_);  # and push(@{$fontHash{$target}}, $_) or $flag=0;
            /^ENDCHAR\b/ and $flag=0 and last; # break out of loop when you hit /ENDCHAR/
        }
    }
    close $fh or die "Error closing file <$f_bitmap>";

    # now manipulate strings so that the array is strings of columns not rows
    my $num_cols = 20;
    my $cur_row_char = "";
    my $cur_row_char_bin;
    my $cur_col_char_bin = "";
    my $cur_col_char; 
    my $cur_col = "";
    my @pixels2;

    for (my $i=0; $i < $num_cols; $i++) #build one column at a time...
    {
        foreach my $row_str (@pixels) #index through each row...
        {
            $cur_row_char = substr $row_str, int($i/4), 1; # grabbing the correct char for the current col
            $cur_row_char_bin = sprintf("%.4b", hex($cur_row_char)); #convert hex char to binary string
            $cur_col_char_bin .= substr $cur_row_char_bin, $i%4, 1; #grab the right binary char 

        }
        $cur_col = sprintf("%.6x", oct("0b${cur_col_char_bin}")); #convert the binary column string to hex
        $pixels2[$i] = $cur_col;
        $cur_col_char_bin = "";
    }
    return \@pixels2;
}

#  createJPCharMap
#  
#  Description: The main driver function for the character encoding. 
#
#  params: none
#  return: \%charMap
#          Reference to the character map Hash holding all vital information
#
sub createJPCharMap
{
    # Convert to unix format or die
    system("dos2unix $f_jptranslations") == 0 
        or die "error converting <$f_jptranslations> to UNIX format!";

    # save string lengths to array and print to file
    @strlens = &getStringLengths;

     # save unique japanese characters to array and print to file 
    print "Getting unique characters...\n";
    &getUniqueCharsUnicode;

    # get ASCII codes for each character 
    print "Getting unique 2-byte ASCII codes...\n";
    &getASCIIkeys;

    # get hex bitmaps for each unique character
    print "Getting JIS from unicode values...\n";
    &getJISfromUnicode;
    

    print "Populating character map...\n";
    my $i=0;
    foreach my $char (@unique_chars)
    {
        # If the JIS font table contains the current value store it, or write N/A if it does not
        my $jis_x = defined $JIS_lookup{$unicodes_x[$i]} ? $JIS_lookup{$unicodes_x[$i]} : "N/A";
        my $jis_d = defined $JIS_lookup{$unicodes_x[$i]} ? hex pop &x2d($jis_x) : "N/A"; # integer value 
        my $bitmap = defined $JIS_lookup{$unicodes_x[$i]} ? &getBitmap($jis_d) : ["BITMAP UNDEFINED"];

        # Store ascii character code, unicode values, JIS values, and bitmap in %charMap{$char} hash array
        $charMap{$char} = [$asciiMap{$char}, $unicodes_d[$i], $unicodes_x[$i], $jis_d, $jis_x, $bitmap];
        $i++;
    }

    # print character data 
    &printJPCharData;

    &makeASCIIstrs;

    return \%charMap; # return reference to character Map
}


#  printJPCharData
#  
#  Description: Subroutine to handle the printing of all output data
#
#  params: none
#  return: none
#
sub printJPCharData
{
#print lengths array to file 
    print "Printing copy-pasteable string lengths to file <$f_jpstringlengths>...\n";
    open my $fh_lengths, ">:utf8", $f_jpstringlengths or die "error opening file: <$f_jpstringlengths>"; 
    print $fh_lengths join ",", @strlens; 
    close $fh_lengths or die "error closing file: <$f_jpstringlengths>"; 

# Print list of unique characters to file 
    print "Printing list of unique characters to file <$f_jpuniquechars>...\n";
    open my $fh_unique, ">:utf8", $f_jpuniquechars or die "error opening file: <$f_jpuniquechars>"; 
    print $fh_unique join("\n",@unique_chars);
    close $fh_unique or die "error closing file: <$f_jpuniquechars>"; 

# Print full character map to file 
    print "Printing entire table of character mappings to file <$f_jpcharmap>...\n";
    open my $fh, ">:utf8", $f_jpcharmap or die "Error opening file <$f_jpcharmap>\n"; 
    foreach my $char (@unique_chars)
    {
        print $fh "CHARACTER{ $char } => "
                        ."\n\tCHARCODE : ".$charMap{$char}[CHARCODE]
                        ."\n\tUNICODE_D: ".$charMap{$char}[UNICODE_D]
                        ."\n\tUNICODE_X: ".$charMap{$char}[UNICODE_X]
                        ."\n\tJIS0208_D: ".$charMap{$char}[JIS_D]
                        ."\n\tJIS0208_X: ".$charMap{$char}[JIS_X]
                        ."\n\tPIXEL_MAP: ". join( ', ', map { "0x".$_} @{$charMap{$char}[BITMAP]} )
                        ." \n\n";
    }
    close $fh or die "error closing file <$f_jpcharmap>";

# Print comma separated bitmaps to file, one character per line. C code copy-pasteable
    print "Printing copy-pasteable bitmaps for each character to file <$f_bitmap_copypaste>...\n";
    open $fh, ">:utf8", $f_bitmap_copypaste or die "Error opening file <$f_bitmap_copypaste>";
    foreach my $char (@unique_chars)
    {
        print $fh join(', ', map { "0x" . $_ } @{$charMap{$char}[BITMAP]}); 
        print $fh ", \/\/ $char\n";
    }
    close $fh or die "error closing file <$f_bitmap_copypaste>\n";
    $| = 0; # STDOUT socket is no longer hot
}

#  makeASCIIstrs
#
#  Description: Writes final encoded strings to output file japanese.txt
#
#  Params: none
#  Return: none
#
sub makeASCIIstrs
{
    print "Printing final encoded japanese strings to file <$f_bitmap_copypaste>...\n";
    open my $fh, ">:utf8", $f_output or die "Error opening file <$f_output>";
    foreach my $str (@strs) 
    {
        foreach my $char ( split('',$str) )
        {
            print $fh "$charMap{$char}[CHARCODE]"; # print each character's ascii index
        }
        print $fh " "; # print space between characters
    }
    close $fh or die "Error closing file <$f_output>";
}
